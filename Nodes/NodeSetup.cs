﻿namespace hayes
{
	
	using UnityEngine;

	#if UNITY_EDITOR
	using UnityEditor;
	#endif



	[ExecuteInEditMode]
	public class NodeSetup : MonoBehaviour
	{

		public enum Tipo
		{
			Area,
			Scout,
			ScoutManager,
			Food,
			FoodManager,
			Point,
			Solution
		}

		public  Tipo _type;

		public string _ID {
			get {
				return ID;
			}

			set {	
				ID = value;			
				this.transform.name = _type.ToString () + " " + value;

				switch (_type) {
				case Tipo.Area:
					SetLayer ("Area");

					#if UNITY_EDITOR 						
					SetLabelIcon (5); 
					#endif

					break;	

				case Tipo.Food:	
					SetLayer ("Food");


				
					#if UNITY_EDITOR 
					SetLabelIcon (3); 
					#endif

					break;	

				case Tipo.FoodManager:		
					SetLayer ("Default");

					#if UNITY_EDITOR 
					SetLabelIcon (3);
					#endif

					break;

				case Tipo.Scout:
					#if UNITY_EDITOR 
					SetLabelIcon (2);
					SetLayer ("Scout");
					#endif
					transform.gameObject.tag = "Scout";
					break;	

				case Tipo.ScoutManager:
					#if UNITY_EDITOR 
					SetLabelIcon (2);
					#endif
					SetLayer ("Default");
					break;	

				case Tipo.Solution: 
					#if UNITY_EDITOR 
					SetLabelIcon (4);
					#endif
					SetLayer ("Solution");
					break;

				case Tipo.Point: 
					#if UNITY_EDITOR 
					SetIcon (3);
					#endif
					SetLayer ("Point");
					transform.gameObject.tag = "Point";



			
					break;	

				default:
					break;
				}

			}
		}


		[Delayed]
		public string ID;
		public bool showGizmos = true;

		[Range (0f, 30f)]
		public float _radius = 10;


		public bool HierarchyError;

		void Awake ()
		{
			#if UNITY_EDITOR
			if (EditorApplication.isPlaying) 
			{
				DestroyImmediate (this);
			}
			#else

				
			DestroyImmediate (this);

			#endif


	


		
	
		}


		void Start ()
		{
			UpdateEditor ();
			
		
		}


		//Updates data on change
		void Update ()
		{	
			UpdateEditor ();
		}



		void UpdateEditor ()
		{
			#if UNITY_EDITOR

			_ID = ID;
			HierarchyError = !IsHierarchyValid ();




			#endif

		}


		void SetLayer (string _layer)
		{
			try {
				this.transform.gameObject.layer = LayerMask.NameToLayer (_layer);

			} catch (System.Exception ex) {
				//DLog.Log ("NodeSetup> Layer not found, create one first before assigning");
	
			}			
		}
			

		public void CreateNode (Tipo t)
		{
			//DLog.Log ("Creating node...");

			GameObject go = new GameObject ();
			NodeSetup n = go.AddComponent<NodeSetup> ();
			SphereCollider t1;
			n._type = t;
			n.showGizmos = false;
			go.transform.SetParent (this.transform);
			go.transform.position = this.transform.position + Random.insideUnitSphere * _radius;
			go.transform.position = new Vector3 (go.transform.position.x,
				this.transform.position.y,			
				go.transform.position.z);
			//Selection.activeGameObject = go;
		
			switch (t) 
			{
			case Tipo.Area:
				go.AddComponent<NodeArea> ();
				break;
			case Tipo.Food:
				go.AddComponent<NodeFoodArea> ();
				go.transform.position = new Vector3 (go.transform.position.x,
					0f,
					go.transform.position.z);
				t1 = go.GetComponent<SphereCollider> ();
				t1.radius = _radius;
				t1.isTrigger = true;
				n.showGizmos = true;			
				break;
			case Tipo.FoodManager:
				go.AddComponent<NodeFoodManager> ();
				go.transform.position = new Vector3 (this.transform.position.x,
					this.transform.position.y + 1f,			
					this.transform.position.z);
				break;
			case Tipo.ScoutManager:
				go.AddComponent<NodeScoutManager> ();
				go.transform.position = new Vector3 (this.transform.position.x,
					this.transform.position.y + 2f,			
					this.transform.position.z);
				break;
			case Tipo.Point:
				go.AddComponent<NodeComida> ();
					 t1 = go.GetComponent<SphereCollider> ();
					t1.radius = 1;
					t1.isTrigger = true;
				go.transform.position = new Vector3 (
					go.transform.position.x,
					0f,
					go.transform.position.z);
				break;

			case Tipo.Scout:
				go.AddComponent<NodeScout> ();
				t1 = go.GetComponent<SphereCollider> ();
				t1.radius = 1;
				t1.isTrigger = true;
				go.transform.position = new Vector3 (go.transform.position.x,
					0f,			
					go.transform.position.z);
				break;
			}	
		


		}
	

		/// <summary>
		/// Determines whether this instance is hierarchy valid.
		/// </summary>
		/// <returns><c>true</c> if this instance is hierarchy valid; otherwise, <c>false</c>.</returns>
		public bool IsHierarchyValid ()
		{

			NodeSetup t = this.transform.parent.GetComponent<NodeSetup> ();

			if (t != null) 
			{
				if (_type == Tipo.Food && t._type == Tipo.FoodManager)
					return true;				
				
				if (_type == Tipo.Scout && t._type == Tipo.ScoutManager)
					return true;	
			
				return false;
			}
			return false;		

		}



		#if UNITY_EDITOR
		void SetLabelIcon (int labelIcon)
		{

			IconManager.ChangeLabelIcon (this.transform.gameObject, labelIcon);

		}


		void SetIcon (int labelIcon)
		{

			IconManager.ChangeIcon (this.transform.gameObject, labelIcon);

		}
		#endif



		#region DEBUG

		public void OnDrawGizmos ()
		{
			if (showGizmos)
				DrawCircle (transform.position, _radius, 30, new Color (1f, 1f, 1f, 0.3f));
		}

		void DrawCircle (Vector3 o, float r, int detail, Color col)
		{
		
			Vector3 prev = new Vector3 (Mathf.Cos (0) * r, 0, Mathf.Sin (0) * r) + o;

			Gizmos.color = col;
			for (int i = 0; i <= detail; i++) {
				float t = (i * Mathf.PI * 2f) / detail;
				Vector3 c = new Vector3 (Mathf.Cos (t) * r, 0, Mathf.Sin (t) * r) + o;
				Vector3 dirToTarget = (c - transform.position).normalized;
				Gizmos.DrawLine (prev, c);


				prev = c;
			}
		
	
		}

		#endregion

	}



}





