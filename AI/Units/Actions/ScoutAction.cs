﻿namespace hayes
{

	using System;
	using UnityEngine;

	public class ScoutAction: Action
	{
		private bool concluido = false;
		private NodeScout targetScout;

		private float startTime = 0;
		public float workDuration = 2;


		public ScoutAction ()
		{	
			Requer ("ClearPath", true);

		
			Efeito ("NeedToScout", false);	
		}


		public override void reseta ()
		{
			concluido = false;
			targetScout = null;
			startTime = 0;
		}

		public override bool jaTerminou ()
		{
			return concluido;
		}

		public override bool requerPresenca ()
		{
			return true;
		}

		public override bool checarPrecondicao (GameObject agent)
		{
			Animal _animal = agent.GetComponent<Animal> ();

			if (_animal.FindScoutNode ())
			{
				targetScout = _animal.status.currentScout;
				target = targetScout.gameObject;
					
				return true;
			}
	

			return false;


		}

		public override bool Efetuar (GameObject agent)
		{
			Animal _animal = agent.GetComponent<Animal> ();


			if (startTime == 0)
				startTime = Time.time;

			if (Time.time - startTime > workDuration) 
			{					
				//stats.needToScout = false;

				targetScout.alreadySearched = true;
				concluido = true;


				if (_animal.ShouldScout ()) 
				{
					return false;
				}


			} 



			return true;
		}

	}
}