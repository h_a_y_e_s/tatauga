﻿namespace hayes
{
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Sense_Sight : MonoBehaviour 
{

	public bool showGizmos = false;

	[HideInInspector]
	public List<Transform> visibleTargets = new List<Transform>();

	public float Intervalo = 1f;

	[Range(0,360)]
	public float viewAngle;
	[Range(0,60)]
	public float viewRadius;

	public LayerMask targetMask;
	public LayerMask obstacleMask;

	Status status;



		void Start()
		{
			StartCoroutine (FindTargetsWithDelay (Intervalo));
			status = GetComponentInParent<Status> ();
		}


		void OnDisable()
		{	
			CancelInvoke ();
			StopAllCoroutines ();

		}
	

	IEnumerator FindTargetsWithDelay(float delay) 	
	{
		while (true) 
		{
			yield return new WaitForSeconds (delay);			
			FindVisibleTargets ();
		}
	}


	void FindVisibleTargets() 
	{
		visibleTargets.Clear ();
		Collider[] targetsInViewRadius = Physics.OverlapSphere (transform.position, viewRadius,targetMask);


		for (int i = 0; i < targetsInViewRadius.Length; i++) 
		{
			Transform target = targetsInViewRadius [i].transform;
			Vector3 dirToTarget = (target.position - transform.position).normalized;
			if (Vector3.Angle (transform.forward, dirToTarget) < viewAngle / 2) 
			{
				float dstToTarget = Vector3.Distance (transform.position, target.position);
				if (!Physics.Raycast (transform.position, dirToTarget, dstToTarget, obstacleMask)) 
				{
					//Debug.Log ("Found " + target.name);
					visibleTargets.Add (target);
					DiscoverNode (target);
				
				}
			}
		}


	}

	void DiscoverNode(Transform _target)
	{
		NodeComida t = _target.GetComponent<NodeComida> ();
		if (t != null)	t.TeamDiscoveredThisNode (status.team);

	}



	#region DEBUG
	public void OnDrawGizmos() 
	{
		if(showGizmos)	DrawLineOfSight (transform.position,viewRadius, 30, new Color(1f,1f,1f,0.3f));
	}

	void DrawLineOfSight (Vector3 o, float r, int detail, Color col) 
	{
	
			Vector3 prev = new Vector3 (Mathf.Cos (0) * r, 0, Mathf.Sin (0) * r) + o;

			Gizmos.color = col;
			for (int i = 0; i <= detail; i++) 
			{
				float t = (i * Mathf.PI * 2f) / detail;
				Vector3 c = new Vector3 (Mathf.Cos (t) * r, 0, Mathf.Sin (t) * r) + o;
				Vector3 dirToTarget = (c - transform.position).normalized;
				Gizmos.DrawLine (prev, c);


				prev = c;
			}
		

		Gizmos.color = col;
		Vector3 viewAngleA = DirFromAngle (-viewAngle / 2, false);
		Vector3 viewAngleB = DirFromAngle (viewAngle / 2, false);
		Gizmos.DrawLine (transform.position, transform.position + viewAngleA * viewRadius);
		Gizmos.DrawLine (transform.position, transform.position + viewAngleB * viewRadius);
		Gizmos.color = Color.blue;

//		//DLog.Log ("Visible Targets:"+visibleTargets.Count );

		foreach (Transform visibleTarget in visibleTargets) 
		{			
			Gizmos.DrawLine (transform.position, visibleTarget.position);
		}
	}


		public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal) {
			if (!angleIsGlobal) {
				angleInDegrees += transform.eulerAngles.y;
			}
			return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad),0,Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
		}
	#endregion



}
}
