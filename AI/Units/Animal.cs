﻿namespace hayes
{
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(Status))]
public abstract class Animal : MonoBehaviour, AI
{	

	public Status status;

	/**
	 * Key-Value data that will feed the  actions and system while planning.
	 */
		public abstract HashSet<KeyValuePair<string,object>> getWorldState ();


	/**
	 * Implement in subclasses
	 */
	public abstract HashSet<KeyValuePair<string,object>> createGoalState ();

	public abstract bool moveAgent (Action nextAction);


	public void planFailed (HashSet<KeyValuePair<string, object>> failedGoal)
	{
		// Not handling this here since we are making sure our goals will always succeed.
		// But normally you want to make sure the world state has changed before running
		// the same goal again, or else it will just fail.
	}

	public void planFound (HashSet<KeyValuePair<string, object>> goal, Queue<Action> actions)
	{
		// Yay we found a plan for our goal
			Debug.Log ("<color=green>"+status.name +" has a Plan! </color> "+Agent.prettyPrint(actions));
	}

	public void actionsFinished ()
	{
		// Everything is done, we completed our actions for this gool. Hooray!
		Debug.Log ("<color=blue>Actions completed</color>");
	}

	public Status getStatus()
	{
		return status;

	}

		public abstract void planAborted (Action aborter);
//	{
//		// An action bailed out of the plan. State has been reset to plan again.
//		// Take note of what happened and make sure if you run the same goal again
//		// that it can succeed.
//		Debug.Log ("<color=red>Plan Aborted</color> "+Agent.prettyPrint(aborter));
//	}


		public enum Nodes
		{
			Food,
			Scout
		}


		// Acha o melhor ponto de comida
		public NodeComida FindBestFoodNode ()
		{

			NodeComida foodPoint = status.currentFood;

			if (status.currentFoodQueue != null)//Tenho uma queue pronta?
			if (status.currentFoodQueue.Count > 0) // A queue tem pelo menos 1 ponto?
			{
				
				if (foodPoint != null) // Existe um food point setado?
				{
					if (foodPoint.Amount > 0)   //Se sim, ele tem comida ainda?
					{
						return status.currentFood; // Se sim, retornar ele
					}
					else // Se não, avançar na Queue
					{
						return status.currentFoodQueue.Dequeue ();					
					}
				}
			}

			//Nao tenho um food point atual, preciso achar um novo
			// Crio uma nova lista de pontos pra visitar
			status.currentFoodQueue.Clear ();

			List<NodeComida>	listaFiltrada = new List<NodeComida> ();
			List<NodeComida> Pontos = status.currentArea.AllPoints.OfType<NodeComida> ().ToList ();

			foreach (var node in Pontos) 
			{

				node.UpdateDistance (status.gameObject.transform.position);
				node.distance += Random.Range (0f, status.RandomLevel);

				if (node.isNodeDiscovered (status.team))
				if (!node.isNodeSearched (status.team))
					listaFiltrada.Add (node);
			}


			status.needToScout = false;

			// Se a lista voltou em branco, esse animal precisa ir Scout pra achar comida
			if (listaFiltrada.Count <= 0) 
			{				
				status.needToScout = true;
				return null;
			}
		

			//Eu criei uma lista, mas uso um Queue, entao hora de fazer uma do jeito dificil
			listaFiltrada = listaFiltrada.OrderBy (y => y.distance).ToList ();

			foreach (var node in listaFiltrada) 
			{
				status.currentFoodQueue.Enqueue (node);
			}

			return status.currentFoodQueue.Dequeue ();



		}

		public bool ShouldScout()
		{
			List<NodeComida>	listaFiltrada = new List<NodeComida> ();
			List<NodeComida> Pontos = status.currentArea.AllPoints.OfType<NodeComida> ().ToList ();

			foreach (var node in Pontos) 
			{
				if (node.isNodeDiscovered (status.team)) listaFiltrada.Add (node);
			}

			// Se a lista voltou em branco, esse animal precisa ir Scout
			// pra achar bons locais que talvez tenham comida.
			if (listaFiltrada.Count <= 0) 
			{				
				status.needToScout = true;
				return true;
			}

			status.needToScout = false;
			return false;
		}


		public NodeScout FindClosestScout ()
		{
			NodeScout scoutPoint =  status.currentScout;

			if (status.currentScoutQueue != null) // Existe um Queue de pontos de Comida?
			if (status.currentScoutQueue.Count > 0) // Ela tem pelo menos 1 ponto valido?
			{
				if (scoutPoint != null)  // Existe um ScoutPoint setado?
				{
					if (!scoutPoint.alreadySearched) // Ele não foi revistado?
					{ 
						return status.currentScout;	//Se ainda nao foi, retorno ele
					} 
					else //Senao continuo para o proximo ponto da Queue 
					{
						return status.currentScoutQueue.Dequeue ();		
					}
				}
			}

			status.currentScoutQueue.Clear ();

			List<NodeScout>	tList = new List<NodeScout> ();
			List<NodeScout> Pontos = status.currentArea.AllPoints.OfType<NodeScout> ().ToList ();

			foreach (var node in Pontos) 
			{

				node.UpdateDistance (status.gameObject.transform.position);
			//	node.distance += Random.Range (0f, status.RandomLevel);


				if (!node.alreadySearched)	tList.Add (node);

			}

			//Eu criei uma lista, mas uso um Queue, entao hora de fazer uma do jeito dificil
			tList = tList.OrderBy (y => y.distance).ToList ();

			foreach (var node in tList) 
			{
				status.currentScoutQueue.Enqueue (node);
			}

			if (status.currentScoutQueue.Count > 0)
				return status.currentScoutQueue.Dequeue ();
			else
				return null;


			
		}






		/// <summary>
		/// Finds the food node.
		/// </summary>
		/// <returns><c>true</c>, if food node was found, <c>false</c> otherwise.</returns>
		public bool FindFoodNode()
		{	
			IsCurrentAreaDone ();		

				if (status.currentArea != null) 
				{	

					NodeComida result =  FindBestFoodNode ();


					if (result != null) 
					{						
						status.currentFood = result;
						return true;
					}
		
				}


			return false;
		}

		public bool FindAnyFoodNode()
		{	
			IsCurrentAreaDone ();		

			List<NodeComida> pontos = status.currentArea.AllPoints.OfType<NodeComida> ().ToList ();

			foreach (var node in pontos) 
			{

				node.UpdateDistance (status.gameObject.transform.position);
				node.distance += Random.Range (0f, status.RandomLevel);		
			}

			//Eu criei uma lista, mas uso um Queue, entao hora de fazer uma do jeito dificil
			pontos = pontos.OrderBy (y => y.distance).ToList ();

			if (pontos [0] != null) 
			{
				status.currentFood = pontos [0];
				return true;
			}	
			return false;
		}



		/// <summary>
		/// Finds the scout node.
		/// </summary>
		/// <returns><c>true</c>, if scout node was found, <c>false</c> otherwise.</returns>
		public bool FindScoutNode()
		{
			IsCurrentAreaDone ();

			if (status.currentArea != null) 
			{				
				NodeScout result =  FindClosestScout ();

				if (result != null) 
				{
					status.currentScout= result;
					return true;
				}
			}	


			return false;

		}



		public void UpdateCurrentActionDisplay(Action a)
		{
			status.currentAction = a;
		}

		public bool isCurrentChallengeSolved()
		{
			if (status.currentChallenge != null)
				return status.currentChallenge.IsSolved;

			return false;

		}




		public void HandleCollision(Collider c)
		{
			if (c.tag == "Desafio") 
			{				
				status.currentChallenge = c.GetComponent<NodeChallenge> ();

				if (status.currentChallenge != null)
				if (!status.currentChallenge.IsSolved) 
				{
					status.IsTheWayBlocked = true;
					Debug.Log (status.name + " found a Challenge!");
				}
			}
		}








		public void IsCurrentAreaDone()
		{
			//Check to see if current Area is done
			if(GameplayManager.instance.isCurrentAreaDone(status))
			{

				//DebugINFO before going to next Area
				Debug.Log( status.name + " has finished " + status.currentArea.name );

				//Advance to next area
				if(status.AreaIndex < (GameplayManager.instance.Areas.Count-1)) status.AreaIndex ++;
				status.currentArea = GameplayManager.instance.getArea (status.AreaIndex);

			}
		}







}
}

