﻿
using UnityEngine;
using System.Collections;
using hayes;

public class CollisionHandler : MonoBehaviour {

	public Turtle turtle;


	public void OnTriggerEnter(Collider c)
	{
		turtle.HandleCollision (c);

	}
}
