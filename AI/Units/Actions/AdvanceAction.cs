﻿namespace hayes
{
	
using UnityEngine;
using System.Collections;

/* Main Aciton that drives the whole TURTLE AI
 * 
 *
 */

public class AdvanceAction :Action 
{

		private bool concluido = false;
		private NodeComida targetFood;

		private float startTime = 0;
		public float workDuration = 2;




		public  AdvanceAction ()
		{
			Requer ("ClearPath", true); 
			Requer ("Sated", true); 

			Efeito ("Advance", true);
		}


		public override void reseta ()
		{
			concluido = false;
			targetFood = null;
			startTime = 0;
		}

		public override bool jaTerminou ()
		{
			return concluido;
		}

		public override bool requerPresenca ()
		{
			return false; 
		}

		public override bool checarPrecondicao (GameObject agent)
		{
			return true;	
		}

		public override bool Efetuar (GameObject agent)
		{

			if (startTime == 0)
				startTime = Time.time;

			if (Time.time - startTime > workDuration) 
			{
				concluido = true;
			} 



			return true;
		}

	}
}

