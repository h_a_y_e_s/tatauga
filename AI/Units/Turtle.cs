﻿namespace hayes
{
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
	//[AddComponentMenu("Hayes/AI/Turtle")]
	[SelectionBase]
	public class Turtle : Animal
	{
	
		//Advance
		public string Goal = "Advance";	
		public BipedMovement movementManager;
		public Memory memory;
		public Sense_Sight visao;


		public override HashSet<KeyValuePair<string,object>> createGoalState () 
		{
			HashSet<KeyValuePair<string,object>> goal = new HashSet<KeyValuePair<string,object>> ();			

			goal.Add(new KeyValuePair<string, object>( Goal, true ));

			return goal;
		}

		public override void planAborted (Action aborted)
		{
			Debug.Log ("<color=red>"+ status.name +"'s plan has been aborted!</color> "+Agent.prettyPrint(aborted	));				
			movementManager.Stop ();			
		}

		public override bool moveAgent(Action nextAction)
		{
			float dist = (gameObject.transform.position - nextAction.target.transform.position).magnitude;	

			if (dist < 2) 
			{				
				nextAction.setInRange (true);
				return true;
			}
			else 
			{
				movementManager.MoveTo (nextAction.target.transform.position);
				return false;
			}
		}

		public override HashSet<KeyValuePair<string,object>> getWorldState ()
		{
			HashSet<KeyValuePair<string,object>> worldData = new HashSet<KeyValuePair<string,object>> ();

			//Advance
			worldData.Add(new KeyValuePair<string, object>("ClearPath", status.isPathClear()));		

			//Food
			worldData.Add(new KeyValuePair<string, object>("Sated", (status.Sated > 50) ));	
			worldData.Add(new KeyValuePair<string, object>("hasFoodInInventory", (status.FoodInInventory > 0) ));	
			worldData.Add(new KeyValuePair<string, object>("NeedToScout", (status.needToScout) ));	

			//challenge 
			worldData.Add(new KeyValuePair<string, object>("HasFoundChallenge", ( status.currentChallenge != null )));	
			worldData.Add(new KeyValuePair<string, object>("IsChallengeComplete", ( isCurrentChallengeSolved() )));	

			return worldData;
		}

	





	}
}
