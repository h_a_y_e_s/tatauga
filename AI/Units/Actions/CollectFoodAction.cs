﻿namespace hayes
{
	
	using System;
	using UnityEngine;

	public class CollectFoodAction : Action
	{
		private bool concluido = false;
		private NodeComida targetFood;

		private float startTime = 0;
		public float workDuration = 2;

		public CollectFoodAction ()
		{			
			Requer ("ClearPath", true);
			Requer ("NeedToScout", false); 		 

			Efeito ("hasFoodInInventory", true);	
		}


		public override void reseta ()
		{
			concluido = false;
			targetFood = null;
			startTime = 0;
		}

		public override bool jaTerminou ()
		{
			return concluido;
		}

		public override bool requerPresenca ()
		{
			return true; 
		}

		public override bool checarPrecondicao (GameObject agent)
		{			
			Animal _animal = agent.GetComponent<Animal> ();

			if (_animal.FindFoodNode ()) 
			{				
				targetFood = _animal.status.currentFood;
				target = targetFood.gameObject;
			}

			return targetFood != null;
			//return true;
			
		}

		public override bool Efetuar (GameObject agent)
		{
			Status stats = agent.GetComponent<Status> ();

			if (startTime == 0)	startTime = Time.time;

			if (Time.time - startTime > workDuration) 
			{
				if (targetFood.Amount > 0) 
				{				
					stats.FoodInInventory += 1;
					targetFood.Amount -= 1;						
				}

				concluido = true;
			} 	


			return true;
		}

	}
}