﻿namespace hayes
{

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EasyEditor;


public class NodeChallenge : Node 
{

		/*SolveChallenge
		 *  Has solution? 
		 *  Yes - Execute solution's action 
		 * 	No - Research new solutions
		 * 		 
		 */
		public Dictionary<Team,NodeStatus> status = new  Dictionary<Team,NodeStatus> ();

		[System.Serializable]
		public class NodeStatus
		{
			public bool hasFood = false;
			public bool hasBeenDiscovered = true; //TEMP
			public bool hasBeenSearched = false;
			public bool playerDiscovered = false;
		}

		public bool IsSolved = false;
		public bool HasBeenFound = true;

		[Range(0,100)]
		public float researchProgress = 0f;

		public Challenge challenge;
	
		public void Initialize()
		{
			
			
		}


		// Goes thru challenge.Solutions and 
		void SetDifficulty()
		{


		}

		public void Reset()
		{
			
		}

		public void Complete(Team team)
		{
			IsSolved = true;


		}


}
	[System.Serializable]
	public class Challenge
	{
		public ChallengeType type;
		public Solution[] solutions;
	}

	[System.Serializable]
	public class Solution
	{		
		public string name;
		public int ID;
		public float difficulty;
		public Acao action;
		public bool IsStuck = false;
	}


	[System.Serializable]
	public class Acao
	{


	}



	public enum ChallengeType
	{
	 	Gate,
		Pit
	}
}
