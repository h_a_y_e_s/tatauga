﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using hayes;

namespace hayes{

	public class TimeManager : MonoBehaviour 
	{
		//Singleton
		public static TimeManager instance;
		List<Status> updateStatus = new List<Status> ();



		[SerializeField]
		public enum Seasons 
		{
			Spring, Summer, Autumm, Winter
		}


		public Seasons seasons;
		public float SeasonsSpeed = 0.005f; //0.005f -> 3m22s cada estacao


		public void Awake()
		{		
			instance = this;

		}

		void Start()
		{
			StartCoroutine(timeTick());

		}

		/// <summary>
		/// Process the passage of time and Seasons 
		/// </summary>
		void ProcessSeasons()
		{
			var t = Mathf.Repeat(Time.time* SeasonsSpeed, 4f);	

			if (t <=4)
				seasons = Seasons.Winter;
			if (t < 3)
				seasons = Seasons.Autumm;
			if (t < 2)
				seasons = Seasons.Summer;
			if (t < 1)
				seasons = Seasons.Spring;


			/*

		if (_debug) 
		{		
			TimeSpan timeSpan = TimeSpan.FromSeconds(Time.timeSinceLevelLoad);
			//string timeText = string.Format("{0:D2}:{1:D2}m:{2:D2}s", timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);
			string timeText = string.Format("{1:D2}m{2:D2}s", timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);
			_debugLine = timeText + " >> " + seasons.ToString ();
		}
		*/

		}

		public void Register(Status s)
		{			
			updateStatus.Add (s);
		}



		IEnumerator timeTick()
		{
			while(true)
			{
				ProcessSeasons ();	

				// ter certeza q todos os donos desses compononets status ainda existemm
				// ou muda esse sistema tosco pra um de eventos

				foreach (var status in updateStatus) 
				{
					status.timeTick ();
					
				}



				yield return new WaitForSeconds(1);



			}
		}

	}
}
