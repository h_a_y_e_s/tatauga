﻿using UnityEngine;
using System.Collections.Generic;
using SmartLocalization;

public class Language : MonoBehaviour 
{
	private LanguageManager languageManager;
	private List<string> currentLanguageKeys;
	private List<SmartCultureInfo> availableLanguages;


	// Use this for initialization
	void Start ()
	{
		languageManager = LanguageManager.Instance;

		SmartCultureInfo deviceCulture = languageManager.GetDeviceCultureIfSupported();
		if(deviceCulture != null)
		{
			languageManager.ChangeLanguage(deviceCulture);	
		}
		else
		{
			////DLog.Log("The device language is not available in the current application. Loading default."); 
		}

		if(languageManager.NumberOfSupportedLanguages > 0)
		{
			currentLanguageKeys = languageManager.GetAllKeys();
			availableLanguages = languageManager.GetSupportedLanguages();
		}
		else
		{
			////DLog.LogError("No languages are created!, Open the Smart Localization plugin at Window->Smart Localization and create your language!");
		}

		LanguageManager.Instance.OnChangeLanguage += OnLanguageChanged;


	}

	void OnLanguageChanged(LanguageManager languageManager)
	{
		////DLog.Log ("Current Language:" + languageManager.CurrentlyLoadedCulture.ToString ());
		currentLanguageKeys = languageManager.GetAllKeys();
	}





	
	// Update is called once per frame
	void Update () {
		
	
	}
}
