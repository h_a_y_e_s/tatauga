﻿namespace hayes
{
	using UnityEngine;
	using EasyEditor;

	[ExecuteInEditMode] 
	public class InspectorManager : MonoBehaviour
	{
		[Inspector(group="Visibility")]
		public bool ShowActions = true;	
		public bool ShowDefaultStuff = false;





		public void Actions ()
		{

			Action[] actions = GetComponents<Action> ();

			if (!ShowActions) {
				foreach (Action a in actions) {
					a.hideFlags = HideFlags.HideInInspector;					
				}
			} else {
				foreach (Action a in actions) {
					a.hideFlags = HideFlags.None;					
				}
			}
		}

	
		void Agent()
		{
			Agent agent = GetComponent<Agent> ();
			if (!ShowDefaultStuff) {

				agent.hideFlags = HideFlags.HideInInspector;					

			} else {

				agent.hideFlags = HideFlags.None;					

			}


		}



		void OnEnable()
		{
			Actions ();
			Agent ();
		}

		void Update()
		{
			if (Application.isPlaying)
				Destroy (this);
			//hideFlags = HideFlags.HideInInspector;

			Actions ();
			Agent ();
		}

		void OnDisable ()
		{

			Action[] actions = GetComponents<Action> ();
			Agent agent = GetComponent<Agent> ();
			agent.hideFlags = HideFlags.None;

			foreach (Action a in actions) 
			{
				a.hideFlags = HideFlags.None;					

			}
			
		}
	}

}