﻿namespace hayes
{
	
using UnityEngine;
using System.Collections;
[System.Serializable]
public class Task
{
		[HideInInspector]
		public int ID = 0;
		public string description;
		public Type type;
		public Estado status;
		public GameObject parent;
}


public enum Estado
{
	NotStarted,
	Started,
	InProgress,
	Done
}

public enum Type
{
	Default,
	Gate,
	Pit

}
}