﻿namespace hayes
{

	using System;
	using UnityEngine;

	public class ConsumeFoodAction : Action
	{
		private bool concluido = false;
		private NodeComida targetFood; 

		private float startTime = 0;
		public float workDuration = 2;

		public ConsumeFoodAction () 
		{
			Requer ("hasFoodInInventory", true); 
			Requer ("Sated", false); 

			Efeito ("Sated", true);	

		}


		public override void reseta ()
		{
			concluido = false;
			targetFood = null;
			startTime = 0;
		}

		public override bool jaTerminou ()
		{
			return concluido;
		}

		public override bool requerPresenca ()
		{
			return false; 

		}

		public override bool checarPrecondicao (GameObject agent)
		{
			Status stats = (Status)agent.GetComponent(typeof(Status));

			if (stats.FoodInInventory > 0)
				return false;
			else return true;
		}

		public override bool Efetuar (GameObject agent)
		{
	
			if (startTime == 0)	startTime = Time.time;

			if (Time.time - startTime > workDuration) 
			{					
				Status stats = agent.GetComponent<Status> ();

				if (stats.FoodInInventory > 0)
				{	
					stats.FoodInInventory -= 1;
					stats.Sated += stats.currentFood.Value; 
				}

				stats.currentFood.TeamSearchedThisNode (stats.team);

				concluido = true;
			} 
	


			return true;
		}

	}
}