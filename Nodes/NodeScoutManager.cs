﻿namespace hayes
{
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class NodeScoutManager : MonoBehaviour {


	public List<NodeScout> AllNodes;
	public List<NodeScout> SortedNodes;

	// Use this for initialization
	void Start () 
	{

		Register ();
	}

	/// <summary>
	/// Register the specified childNode.
	/// </summary>
	/// <param name="childNode">Child node.</param>
	void Register()
	{
		AllNodes = GetComponentsInChildren<NodeScout> ().ToList();
	}
}
}