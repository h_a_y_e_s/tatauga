﻿namespace hayes
{
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using EasyEditor;

public class Status : MonoBehaviour 
{	
	[Inspector(group = "Status")]
		public int ID = 1;
		public string name = "";
		public Team team;
		public float HP = 100f;
		public float Energy = 100f;	

		[ReadOnly]
		public float Sated=25f;	
		[ReadOnly]
		public float Happiness, FearLevel =50f;
		[ReadOnly]
		public float FoodInInventory;

		[Inspector(group = "Flags")]
		[ReadOnly]
		public bool needToScout = true;
		[Inspector(group = "Flags")][ReadOnly]
		public bool IsTheWayBlocked = false;

		[Inspector(group = "Personality")]

		public float Destemido, Resiliente, Confiante, Inteligencia, LoneWolf;
		public float CompletionistLevel= 100f;	
		public float RandomLevel= 20f;

		[Inspector(group = "Settings")]
		public float SatedDecayRate = 0.01f;

	
		[Inspector(group = "Task")] [SerializeField]
		public int AreaIndex = 0;
		public NodeArea currentArea;
		//public NodeComida defaultFood;
		public NodeComida currentFood;
		public NodeScout currentScout;
		public Action currentAction;

		public NodeChallenge currentChallenge;
		public Solution currentSolution;

		[SerializeField]
		public Queue<NodeComida> currentFoodQueue = new Queue<NodeComida>();
		[SerializeField]
		public Queue<NodeScout> currentScoutQueue = new Queue<NodeScout>();

		[Inspector(group = "Inventory")] [SerializeField] 
		public Dictionary<int,int> inventory = new  Dictionary<int,int> ();

		[Inspector(group = "Inventory")]	
		public List<int> keys = new List<int>();




	public void timeTick()
	{			
			Sated -= SatedDecayRate;			
	}

	public bool isPathClear()
	{
			if (IsTheWayBlocked) return false;
			
			if (currentChallenge != null) if(!currentChallenge.IsSolved) return false;	
			
			return true;
	}


	public void CompleteCurrentChallenge()
	{
		currentChallenge.Complete (team);
		IsTheWayBlocked = false;
	}

	public void Start()
	{
		//Register to get ticks from time manager
		TimeManager.instance.Register (this);
		ID = GetInstanceID ();
		currentArea = GameplayManager.instance.getArea (AreaIndex);
	}


	#region InventoryFunctions

	public bool HasKey(int keyID)
	{
		return keys.Contains(keyID);
	}

	public void AddKey(int keyID)
	{
		keys.Add(keyID);
	}

	public void RemoveKey(int keyID)
	{
		keys.Remove(keyID);
	}

	public void AddItem(int keyID,int amount)
	{
			inventory.Add(keyID,amount);
	}

	public void RemoveItem(int keyID)
	{
		inventory.Remove(keyID);
	}

	public bool HasItem(int keyID)
	{
		return inventory.ContainsKey (keyID);
	}
		#endregion

}
}