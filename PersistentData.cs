﻿using UnityEngine;
using System.Collections;
using System.IO;
using LitJson;

public class PersistentData 
{
	public static void Save(object data, string filename)
	{		
		try {
			var ToWrite = JsonUtility.ToJson (data);
			File.WriteAllText (Application.dataPath + "/Resources/Data/" + filename + ".json", ToWrite);
		} catch (System.Exception ex) 
		{
			
		}
	}


	public static JsonData Read(string filename)
	{	
		try {
			TextAsset textAsset = Resources.Load<TextAsset> ("Data/" + filename);
			JsonData data = JsonMapper.ToObject (textAsset.text);
			return data;
		} catch (System.Exception ex) 
		{
			return null;
		}
	}

}
