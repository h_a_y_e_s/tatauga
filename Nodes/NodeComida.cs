﻿namespace hayes
{
	using UnityEngine;
	using System.Collections;
	using System.Collections.Generic;

	using DG.Tweening;
	using DG.Tweening.Core;
	using DG.Tweening.Core.Easing;
	using DG.Tweening.Plugins;

	using EasyEditor;

	[System.Serializable]
	public class NodeComida : Node
	{

		public Team currentTeam;

		public Dictionary<Team,NodeStatus> status = new  Dictionary<Team,NodeStatus> ();

	
		[System.Serializable]
		public class NodeStatus
		{
			public bool hasFood = false;
			public bool hasBeenDiscovered = false; //TEMP
			public bool hasBeenSearched = false;
			public bool playerDiscovered = false;
		}

		#if UNITY_EDITOR

		//[Inspector (group = "Inspector")]
		void Update()
		{
			if (status.ContainsKey (InspectorTeam))
			{
				hasBeenDiscovered = status [Team.Turtle].hasBeenDiscovered;
				hasBeenSearched = status [Team.Turtle].hasBeenSearched;
			} else
			{ 
				hasFood=hasBeenDiscovered=hasBeenSearched=playerDiscovered =false; 
			}


		}

		[Inspector(group = "Inspector")]
		public Team InspectorTeam;
		[Inspector(group = "Inspector"),ReadOnly][SerializeField]
		bool hasFood, hasBeenDiscovered,hasBeenSearched,playerDiscovered;	 


		#endif

		[Inspector(group = "Nutrition Info")]
		public string Name = "NO FOOD";
		public float Value = 3;
		public int Amount = 3;
		public float Fat = 1;
		public  float Energy = 2;



		new void Start ()
		{
			base.Start ();
		
		}

		public bool isNodeSearched(Team _team)
		{	
			if(status.ContainsKey(_team))
			if (status[_team].hasBeenSearched) return true;	

			return false;
		}

		public bool isNodeDiscovered(Team _team)
		{	
			if(status.ContainsKey(_team))
			if (status[_team].hasBeenDiscovered )
				return true;	


			return false;
		}

		public void TeamDiscoveredThisNode(Team _team)
		{
			if (!status.ContainsKey (_team)) 
			{	
				NodeStatus newNS = new NodeStatus ();
				status.Add (_team, newNS);
				status [_team].hasBeenDiscovered = true;
			} else 
			{
				status [_team].hasBeenDiscovered = true;
			}
			
		}

		public void TeamSearchedThisNode(Team _team)
		{
			if (!status.ContainsKey (_team)) 
			{	
				NodeStatus newNS = new NodeStatus ();
				status.Add (_team, newNS);
				status [_team].hasBeenSearched = true;
			} else 
			{
				status [_team].hasBeenSearched = true;
			}

		}



		public void InstantiateModel ()
		{
			/* OUTDATED
			if (food != null) 
			{
				int randomPrefab = Random.Range (0, food.prefabs.Length);		
				
				GameObject go = (GameObject)Instantiate (food.prefabs [randomPrefab],	transform.position, Quaternion.identity);
				
				var euler = go.transform.eulerAngles;
				euler.y = Random.Range (0f, 360f);
				go.transform.eulerAngles = euler;
				go.transform.SetParent (this.transform);
				food.prefabs = new GameObject[]{ go };
				hasFood = true;
				//	go.transform.transform.localScale = new Vector3 (0f, 0f, 0f);
				//go.transform.DOScale (new Vector3 (1f, 1f, 1f), 1f);
			}*/
		}

		public void ConsumeFood ()
		{
			/* OUTDATED
			if (food.Amount > 0) 
			{
				food.Amount--;
			}
			else 
			{
				hasFood = false;
				DestroyPoint ();

			}
			*/
		}


		public void DestroyPoint ()
		{
			/* OUTDATED
			//temp 
			if(food.prefabs.Length>0)
			food.prefabs[0].transform.DOScale (new Vector3 (0f, 0f, 0f), 5f).OnComplete (GrowAnimation);
			*/

		}





	}
}
