﻿
namespace hayes
{
using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public abstract class Action : MonoBehaviour {


		private HashSet<KeyValuePair<string,object>> requerimentos;
		private HashSet<KeyValuePair<string,object>> efeitos;

	private bool inRange = false;

	/* The cost of performing the action. 
	 * Figure out a weight that suits the action. 
	 * Changing it will affect what actions are chosen during planning.*/
	public float cost = 1f;

	/**
	 * An action often has to perform on an object. This is that object. Can be null. */
	public GameObject target;

	public Action() 
	{
		requerimentos = new HashSet<KeyValuePair<string, object>> ();
		efeitos = new HashSet<KeyValuePair<string, object>> ();
	}

	public void doReset() {
		inRange = false;
		target = null;
		reseta ();
	}

	/**
	 * Reset any variables that need to be reset before planning happens again.
	 */
	public abstract void reseta();

	/**
	 * Is the action done?
	 */
	public abstract bool jaTerminou();

	/**
	 * Procedurally check if this action can run. Not all actions
	 * will need this, but some might.
	 */
	public abstract bool checarPrecondicao(GameObject agent);

	/**
	 * Run the action.
	 * Returns True if the action performed successfully or false
	 * if something happened and it can no longer perform. In this case
	 * the action queue should clear out and the goal cannot be reached.
	 */
	public abstract bool Efetuar(GameObject agent);

	/**
	 * Does this action need to be within range of a target game object?
	 * If not then the moveTo state will not need to run for this action.
	 */
	public abstract bool requerPresenca ();


	/**
	 * Are we in range of the target?
	 * The MoveTo state will set this and it gets reset each time this action is performed.
	 */
	public bool isInRange () {
		return inRange;
	}

	public void setInRange(bool inRange) {
		this.inRange = inRange;
	}


	public void Requer(string key, object value) {
		requerimentos.Add (new KeyValuePair<string, object>(key, value) );
	}


	public void removePrecondition(string key) {
		KeyValuePair<string, object> remove = default(KeyValuePair<string,object>);
		foreach (KeyValuePair<string, object> kvp in requerimentos) {
			if (kvp.Key.Equals (key)) 
				remove = kvp;
		}
		if ( !default(KeyValuePair<string,object>).Equals(remove) )
			requerimentos.Remove (remove);
	}


	public void Efeito(string key, object value)
	{
		efeitos.Add (new KeyValuePair<string, object>(key, value) );
	}


	public void removeEffect(string key) {
		KeyValuePair<string, object> remove = default(KeyValuePair<string,object>);
		foreach (KeyValuePair<string, object> kvp in efeitos) {
			if (kvp.Key.Equals (key)) 
				remove = kvp;
		}
		if ( !default(KeyValuePair<string,object>).Equals(remove) )
			efeitos.Remove (remove);
	}


	public HashSet<KeyValuePair<string, object>> Preconditions {
		get {
			return requerimentos;
		}
	}

	public HashSet<KeyValuePair<string, object>> Effects {
		get {
			return efeitos;
		}
	}


		public NodeComida findClosest(GameObject agent)
		{
			NodeComida[] foods = (NodeComida[])UnityEngine.GameObject.FindObjectsOfType (typeof(NodeComida));
			NodeComida closest = null;
		
			float closestDist = 0;

			foreach (NodeComida food in foods)
			{
				if (closest == null) 
				{
					// first one, so choose it for now
					closest = food;
					closestDist = (food.gameObject.transform.position - agent.transform.position).magnitude;
				} 
				else 
				{			
					float dist;
					if (food.Amount > 0){ dist = (food.gameObject.transform.position - agent.transform.position).magnitude;	}
					else{ dist = 9999;}


					if (dist < closestDist) 
					{						
						closest = food;
						closestDist = dist;
					}			

				}
			}

			return  closest;
			
		}
}
}