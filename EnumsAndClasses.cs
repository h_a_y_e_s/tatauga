﻿namespace hayes
{	
	using UnityEngine;

	// Teams share knowledge, such as food locations.
	[System.Serializable]
	public enum Team
	{
		Turtle,
		Turtles,
		Bird,
		Birds,
		Enemies,
		All
	}

	#region Food

	[System.Serializable]
	public class Food
	{
		public string name;
		public Category category = new Category();

		public float Amount;
		[HideInInspector]
		private float TAmount;
		public float Energy;
		public float Fat;
		public GameObject[] prefabs = new GameObject[]{};



		public static Food Clone(Food _food)
		{
			Food _clone = new Food ();
			_clone.name = _food.name;
			_clone.TAmount =_clone.Amount = _food.Amount;
			_clone.Energy = _food.Energy;
			_clone.Fat = _food.Fat;
			_clone.prefabs = _food.prefabs;
			return _clone;
		}

		public void Reset()
		{
			Amount = TAmount;

		}
	}

	[System.Serializable]
	public enum Category
	{
		Flowers,
		Fruits,
		Vegetables
	}

	[System.Serializable]
	public enum CategoryPoint
	{
		Flowers,
		Fruits,
		Vegetables,
		None,
		Scout
	}

	[System.Serializable]
	public class CategoriesToSpawn
	{
		public Category category;
		[Range(0,100)]
		public float percentage = 1f;
		[HideInInspector]
		public int amount = 0;
	}
	#endregion

	#region AI

	public enum Interaction
	{

		Idle, 
		Scout,
		SearchForFood


	}

	[System.Serializable]
	public class Task1
	{
		public enum Type2
		{
			Default,
			SearchForFood,
			SearchForSolution,
			SolveChallenge,
			Gathering
		}


		public enum SubTask
		{
			None,
			MoveTo,
			Search
		}

		public enum Progress
		{
			NotStarted,
			Halted,
			Start,
			InProgress,
			Done
		}

		public enum Priority
		{
			Low,
			Default,
			High,
			Extreme

		}

		public Type2 type;
		public SubTask subTask;
		public Progress progress;
		public Priority priority;


		public Task1 ()
		{

			type = Type2.Default;
			subTask = SubTask.None;
			progress = Progress.NotStarted;
			priority = Priority.Default;

		}

	}

	#endregion
}

