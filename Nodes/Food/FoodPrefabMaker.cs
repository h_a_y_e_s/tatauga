﻿namespace hayes
{
	using UnityEngine;
	using System.Collections;
	using System.Collections.Generic;
	using System.IO;
	using LitJson;

	public class FoodPrefabMaker : MonoBehaviour
	{

		public Food[] foodPrefabs;
		JsonData prefabsData;

		// Use this for initialization
		void Start ()
		{
			
		}

		public void SaveToJson()
		{
			prefabsData = JsonMapper.ToJson (foodPrefabs);
			File.WriteAllText (Application.dataPath + "/Data/FoodPrefabs.json", prefabsData.ToString());


		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

	}
}