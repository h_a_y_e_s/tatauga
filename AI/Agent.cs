﻿namespace hayes
{
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using EasyEditor;


public sealed class Agent : MonoBehaviour 
{

	private FSM stateMachine;

	private FSM.FSMState idleState; // finds something to do
	private FSM.FSMState moveToState; // moves to a target
	private FSM.FSMState performActionState; // performs an action

	private HashSet<Action> availableActions;
	private Queue<Action> acoesAtuais;
	

	public AI NPC_AI; // this is the implementing class that provides our world data and listens to feedback on planning

	private Planner planner;


	void Start ()
	{

		stateMachine = new FSM ();
		availableActions = new HashSet<Action> ();
		acoesAtuais = new Queue<Action> ();
		planner = new Planner ();


		findDataProvider ();
		createIdleState ();
		createMoveToState ();
		createPerformActionState ();

		stateMachine.pushState (idleState);



		loadActions ();
	}


	void Update ()
	{
		stateMachine.Atualizar (this.gameObject);

		// Shows on the status the current action
		if(acoesAtuais.Count > 0)  NPC_AI.UpdateCurrentActionDisplay( acoesAtuais.Peek ());
		
	}


	public void addAction(Action a) 
	{
		availableActions.Add (a);
	}

	public Action getAction(Type action) 
	{
		foreach (Action g in availableActions) {
			if (g.GetType().Equals(action) )
				return g;
		}
		return null;
	}

	public void removeAction(Action action)
	{
		availableActions.Remove (action);
	}

	private bool hasActionPlan() 
	{
		return acoesAtuais.Count > 0;
	}

	private void createIdleState() {
		idleState = (fsm, gameObj) => 
		{
			//  planning

			// get the world state and the goal we want to plan for		
			HashSet<KeyValuePair<string,object>> worldState = NPC_AI.getWorldState();
			HashSet<KeyValuePair<string,object>> goal = NPC_AI.createGoalState();

			// Plan
				Queue<Action> novoPlano = planner.plan(gameObject, availableActions, worldState, goal);
			if (novoPlano != null) 
			{
				// we have a plan, hooray!
				acoesAtuais = novoPlano;
				NPC_AI.planFound(goal, novoPlano);

				fsm.popState(); // move to PerformAction state
				fsm.pushState(performActionState);

			} else
			{
				// ugh, we couldn't get a plan
				Debug.Log("<color=orange>"+NPC_AI.getStatus().name +"'s plan has FAILED!: </color>"+prettyPrint(goal));
				NPC_AI.planFailed(goal);
				fsm.popState (); // move back to IdleAction state
				fsm.pushState (idleState);
			}

		};
	}

	private void createMoveToState() 
	{
			
		moveToState = (fsm, gameObj) => {


			// move the game object

			Action action = acoesAtuais.Peek();
			if (action.requerPresenca() && action.target == null) 
			{
				Debug.Log("<color=red>Fatal error:</color> Action requires a target but has none. Planning failed. You did not assign the target in your Action.checkProceduralPrecondition()");
				fsm.popState(); // move
				fsm.popState(); // perform
				fsm.pushState(idleState);
				return;
			}

			if( !NPC_AI.getStatus().isPathClear())
			{					
				//Debug.Log(NPC_AI.getStatus().name + "'s way is blocked!");

				NPC_AI.planAborted(action);

				fsm.popState(); // move
				fsm.popState(); // perform
				fsm.pushState(idleState);
				return;
			}			

			if ( NPC_AI.moveAgent(action))
			{					
				fsm.popState();
			}

		};
	}

	private void createPerformActionState() {

		performActionState = (fsm, gameObj) => {
			// perform the action
	

			if (!hasActionPlan()) {
				// no actions to perform
				Debug.Log("<color=red>Done actions</color>");
				fsm.popState();
				fsm.pushState(idleState);
				NPC_AI.actionsFinished();
				return;
			}

				Action acao = acoesAtuais.Peek();
			if ( acao.jaTerminou() )
			{
				// the action is done. Remove it so we can perform the next one
				acoesAtuais.Dequeue();
			}

			if (hasActionPlan())
			{
				// perform the next action
				acao = acoesAtuais.Peek();
				bool inRange = acao.requerPresenca() ? acao.isInRange() : true;

				if ( inRange ) 
				{
					// we are in range, so perform the action
					bool success = acao.Efetuar(gameObj);

					if (!success)
					{
						// action failed, we need to plan again
						fsm.popState();
						fsm.pushState(idleState);
						NPC_AI.planAborted(acao);
					}
				} else {
					// we need to move there first
					// push moveTo state
					fsm.pushState(moveToState);
				}

			}
			else 
				{
				// no actions left, move to Plan state
				fsm.popState();
				fsm.pushState(idleState);
				NPC_AI.actionsFinished();
			}

		};
	}

	private void findDataProvider() {
		foreach (Component comp in gameObject.GetComponents(typeof(Component)) ) {
			if ( typeof(AI).IsAssignableFrom(comp.GetType()) ) {
				NPC_AI = (AI)comp;
				return;
			}
		}
	}



	public void   AddActionComponent()
	{
			
	}



	[Inspector]
	private void loadActions ()
	{
		Action[] actions = gameObject.GetComponents<Action>();
		foreach (Action a in actions)
		{
			availableActions.Add (a);
		}
			Debug.Log(NPC_AI.getStatus().name + " has the following actions:  "+prettyPrint(actions)+ ".");
	}	



		#region PrettyPrint

	public static string prettyPrint(HashSet<KeyValuePair<string,object>> state) {
		String s = "";
		foreach (KeyValuePair<string,object> kvp in state) {
			s += kvp.Key + ":" + kvp.Value.ToString();
			s += ", ";
		}
		return s;
	}

	public static string prettyPrint(Queue<Action> actions) {
		String s = "";
		foreach (Action a in actions) 
		{
			s += a.GetType().Name;
			s += "-> ";
		}
		s += "<end>";
		s = s.Replace ("Action", "");
		return s;
	}

	public static string prettyPrint(Action[] actions) {
		String s = "";
		foreach (Action a in actions) {
			s += a.GetType().Name;
			s += ", ";			
		}
//			s += "<end>";
			s = s.Remove (s.Length - 2);
			s = s.Replace ("Action", "");
		return s;
	}

	public static string prettyPrint(Action action) {
		String s = ""+action.GetType().Name;
		return s;
	}
		#endregion
}
}