﻿using System;
using Apex;
using Apex.Steering;
using Apex.Steering.Components;
using Apex.PathFinding;
using Apex.Services;
using Apex.Units;
using UnityEngine;
using DG.Tweening;
using DG.Tweening.Plugins;

public class BipedMovement : MonoBehaviour,IMoveUnits
{
	public IUnitFacade _unit;
	//public Transform target;
	public float animatorSpeed =  1f;
	private Transform _transform;
	private Animator _animator;
	private int _speedId;
	private int _angleId;
	private int _PegarComidaId;
	private int	_ProcurarSemComidaId;

	public  PathResult pr;



	// Use this for initialization
	void Start ()
	{
		_animator = GetComponentInChildren<Animator> ();
		_unit = this.GetUnitFacade();
		_transform = transform;
		_animator.speed = animatorSpeed;
		_speedId = Animator.StringToHash ("Forward");
		_angleId = Animator.StringToHash ("Turn");
		_PegarComidaId= Animator.StringToHash ("Comer");
		_ProcurarSemComidaId = Animator.StringToHash ("SemComida");

	}

	/// <summary>
	/// Moves to.
	/// </summary>
	/// <param name="pos">Position.</param>
	public void MoveTo(Vector3 pos)
	{
		//		target.transform.DOMove (pos,0.1f);
		_unit.MoveTo (pos, false);
	}

	public void PegarComida()
	{
		_animator.SetTrigger (_PegarComidaId);
	}

	public void ProcurarSemComida()
	{
		_animator.SetTrigger (_ProcurarSemComidaId);
	}


	/// <summary>
	/// Moves the unit by the specified velocity.
	/// </summary>
	/// <param name="velocity">The velocity.</param>
	/// <param name="deltaTime">Time since last invocation in ms</param>
	public void Move(Vector3 velocity, float deltaTime)
	{
		float speed = velocity.magnitude;
		_animator.SetFloat(_speedId, speed);

		if (speed < 0.2f)
		{
			_animator.SetFloat(_speedId, 0f);
			_animator.SetFloat(_angleId, 0f);
			return;
		}

		float angleDirection = TurnDir(_transform.forward, velocity);
		float angle = Vector3.Angle (_transform.forward, velocity)*angleDirection;

		_animator.SetFloat(_speedId, speed);
		_animator.SetFloat(_angleId, angle);

	}

	public  void Rotate(Vector3 targetOrientation, float angularSpeed, float deltaTime)
	{



	}



	private static float TurnDir(Vector3 p1, Vector3 p2)
	{
		return Mathf.Sign ((p1.z*p2.x)-(p1.x* p2.z));
	}

	/// <summary>
	/// Wait the specified lenght.
	/// </summary>
	/// <param name="lenght">Lenght.</param>
	public void Wait(float? lenght)
	{
		_unit.Wait (lenght);
	}

	public void Stop()
	{
		_animator.SetFloat (_speedId, 0f);
		_unit.Stop();
	}

	/// <summary>
	/// Resume this instance.
	/// </summary>
	public void Resume()
	{
		_unit.Resume ();

	}

	/// <summary>
	/// Requests the path cost.
	/// </summary>
	public void RequestPathCost(Vector3 testTarget)
	{
		//var unit = this.GetUnitFacade();
		var req = _unit.CreatePathRequest(testTarget, callback);
		req.type = RequestType.IntelOnly;
		GameServices.pathService.QueueRequest (req);
	}

	/// <summary>
	/// The callback.
	/// </summary>
	Action<PathResult> callback = (result) =>
	{
		//Here we treat partial completion the same as full completion, you may want to treat partials differently
		if (!(result.status == PathingStatus.Complete || result.status == PathingStatus.CompletePartial))
		{	
			return;
		}

		//		MovementManager.instance.pr = result;
		Debug.Log("Cost of path was: " + result.pathCost);
	};


}
