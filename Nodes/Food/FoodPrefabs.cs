﻿namespace hayes
{
	using UnityEngine;
	using System.Collections;
	using System.Collections.Generic;

	public class FoodPrefabs : MonoBehaviour
	{
		#region Singleton

		public static FoodPrefabs instance;

		void Awake ()
		{
			instance = this;	
		}

		#endregion


		public List<Food> foodPrefabs =  new List<Food>();	

		public List<Food> GetPrefabsOfType(Category c)
		{
			List<Food> t = new List<Food> ();

			foreach (var item in foodPrefabs)
			{
				if (item.category == c)
					t.Add (item);
			}

			return t;		
		}

	}
}
