﻿namespace hayes
{
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

	[RequireComponent(typeof(SphereCollider))][System.Serializable]
public class Node : MonoBehaviour 
{
		[HideInInspector]
		public float distance = 0f;

		public void Start()
		{
			SphereCollider t =	GetComponent<SphereCollider> ();
			t.isTrigger = true;
		}
	
		public void UpdateDistance(Vector3 a)
		{
			Vector3 offset = transform.position - a;
			distance = offset.sqrMagnitude;
		}

}
}