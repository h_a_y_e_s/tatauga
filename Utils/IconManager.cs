﻿// Code based on http://answers.unity3d.com/questions/213140/programmatically-assign-an-editor-icon-to-a-game-o.html

namespace hayes
{
	
using System;
using System.Reflection;

#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public static class IconManager
{
		#if UNITY_EDITOR
	/// <summary>
	/// Label icon.
	/// </summary>
	public enum LabelIcon {
		Gray = 0,
		Blue,
		Teal,
		Green,
		Yellow,
		Orange,
		Red,
		Purple
	}
	/// <summary>
	/// Icon.
	/// </summary>
	public enum Icon {
		CircleGray = 0,
		CircleBlue,
		CircleTeal,
		CircleGreen,
		CircleYellow,
		CircleOrange,
		CircleRed,
		CirclePurple,
		DiamondGray,
		DiamondBlue,
		DiamondTeal,
		DiamondGreen,
		DiamondYellow,
		DiamondOrange,
		DiamondRed,
		DiamondPurple
	}
	/// <summary>
	/// Changes the label icon.
	/// </summary>
	/// <param name="gameObject">Game object.</param>
	/// <param name="idx">Index.</param>
	public static void ChangeLabelIcon(GameObject gameObject, int idx)
	{
		
		var largeIcons = GetTextures("sv_label_", string.Empty, 0, 8);
		var icon = largeIcons[(int)idx];
		var egu = typeof(EditorGUIUtility);
		var flags = BindingFlags.InvokeMethod | BindingFlags.Static | BindingFlags.NonPublic;
		var args = new object[] { gameObject, icon.image };

		var setIcon = egu.GetMethod("SetIconForObject", flags, null, new System.Type[]{typeof(UnityEngine.Object), typeof(Texture2D)}, null);
		setIcon.Invoke(null, args);

	}
	/// <summary>
	/// Changes the icon.
	/// </summary>
	/// <param name="gameObject">Game object.</param>
	/// <param name="idx">Index.</param>
	public static void ChangeIcon(GameObject gameObject, int idx)
	{
		var largeIcons = GetTextures("sv_icon_dot","_pix16_gizmo", 0, 8);
		var icon = largeIcons[idx];
		var egu = typeof(EditorGUIUtility);
		var flags = BindingFlags.InvokeMethod | BindingFlags.Static | BindingFlags.NonPublic;
		var args = new object[] { gameObject, icon.image };

		var setIcon = egu.GetMethod("SetIconForObject", flags, null, new System.Type[]{typeof(UnityEngine.Object), typeof(Texture2D)}, null);
		setIcon.Invoke(null, args);

	}

	

	/// <summary>
	/// Gets the textures.
	/// </summary>
	/// <returns>The textures.</returns>
	/// <param name="baseName">Base name.</param>
	/// <param name="postFix">Post fix.</param>
	/// <param name="startIndex">Start index.</param>
	/// <param name="count">Count.</param>
	private static GUIContent[] GetTextures(string baseName, string postFix, int startIndex, int count)
	{
		GUIContent[] array = new GUIContent[count];
		for (int i = 0; i < count; i++)
		{
			array[i] = EditorGUIUtility.IconContent(baseName + (startIndex + i) + postFix);
		}
		return array;
	
	}

		#endif
}

}



