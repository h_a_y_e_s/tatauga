﻿namespace hayes
{
	
	using System;
	using UnityEngine;
	// Blank Action to connect Scout to Collect
	// Perform always fails and causes to Replan
	public class ForceReplan : Action
	{
		private bool IsFinished = false;
		private NodeComida targetFood;

		public ForceReplan ()
		{			
			Requer ("ClearPath", true);
			Requer ("NeedToScout", false); 		 

			Efeito ("hasFoodInInventory", true);	
		}


		public override void reseta ()
		{
			IsFinished = false;
			targetFood = null;

		}

		public override bool jaTerminou ()
		{
			return IsFinished;
		}

		public override bool requerPresenca ()
		{
			return false; 
		}

		public override bool checarPrecondicao (GameObject agent)
		{
			return true;
			
		}

		public override bool Efetuar (GameObject agent)
		{
			return false;
		}

	}
}