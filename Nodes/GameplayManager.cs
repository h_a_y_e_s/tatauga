﻿namespace hayes
{
	using UnityEngine;
	using System.Collections;
	using System.Collections.Generic;
	using System.Linq;

	public class GameplayManager : MonoBehaviour
	{
		public static GameplayManager instance;	
		public List<NodeArea> Areas = new List<NodeArea> ();

		void Awake ()
		{
			instance = this;
			//InitializeAreas ();		
		}

		public NodeArea getArea(int AreaIndex)
		{
			if (AreaIndex < Areas.Count - 1) {
			//	Debug.Log ("Area Index >>" + AreaIndex + "Areas.Count:" + (Areas.Count - 1));
				return Areas [AreaIndex];
			} else 
			{
				//Debug.Log (">Area Index >>" + AreaIndex + "Areas.Count:" + (Areas.Count - 1));
				return Areas [Areas.Count - 1];
			}
		}

		void InitializeAreas ()
		{
			Areas = GetComponentsInChildren<NodeArea> ().ToList ();		
		}

		public bool isCurrentAreaDone(Status _status)
		{
			var foodPoints = Areas[_status.AreaIndex].GetComponentsInChildren<NodeComida> ();
			int result = 0;

			foreach(var point in foodPoints)
			{
				if (point.isNodeSearched(_status.team)) result++;
			}

			float tshreshold = (float)foodPoints.Length * (_status.CompletionistLevel / 100);
			//Debug.Log ("Completionist:" + tshreshold.ToString () + "/ Foodpoints:"+foodPoints.Length  );

			if (result < tshreshold) return false;	
			else 	return true;		
		}




	
	}
}