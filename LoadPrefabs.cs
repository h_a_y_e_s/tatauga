﻿using UnityEngine;
using System.Collections;

public class LoadPrefabs : MonoBehaviour
{
	void Awake ()
	{
		#if UNITY_ANDROID
		if (!Application.isEditor) 
		{
			//Screen.SetResolution (1440, 900, true);
			Application.targetFrameRate = 50;
			Screen.sleepTimeout = SleepTimeout.NeverSleep;
			StartCoroutine(WaitForScreenChange(true));
		}
		#endif
		Resources.LoadAll ("Prefabs/");

	}

	private IEnumerator WaitForScreenChange(bool fullscreen)
	{
		int width = 1440;
		int height = 900;

		Screen.fullScreen = fullscreen;

		yield return new WaitForEndOfFrame();
		yield return new WaitForEndOfFrame();

		Screen.SetResolution(width, height, Screen.fullScreen);
	}


	


}
