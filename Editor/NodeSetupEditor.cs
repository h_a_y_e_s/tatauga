﻿namespace hayes
{
	using UnityEngine;
	using UnityEditor;
	using System.Collections;

	[CustomEditor (typeof(NodeSetup), false), CanEditMultipleObjects]
	public partial class NodeSetupEditor:Editor
	{
		NodeSetup ns;

		private SerializedProperty _type;
		private SerializedProperty _ID;
		private SerializedProperty ID;
		private SerializedProperty showGizmos;
		private SerializedProperty _radius;
		//	private SerializedProperty _area;
		//	private SerializedProperty _areaManager;
		//	private SerializedProperty _food;
		//	private SerializedProperty _foodManager;
		//	private SerializedProperty _scout;
		//	private SerializedProperty _scoutManager;
		//	private SerializedProperty _solution;
		private SerializedProperty HierarchyError;
		//	private SerializedProperty _point;



		void OnEnable ()
		{
			ns = (NodeSetup)target;

			_type = this.serializedObject.FindProperty ("_type");
			_ID = this.serializedObject.FindProperty ("_ID");
			ID = this.serializedObject.FindProperty ("ID");
			showGizmos = this.serializedObject.FindProperty ("showGizmos");
			_radius = this.serializedObject.FindProperty ("_radius");
//		_area  = this.serializedObject.FindProperty("_area");
//		_food  = this.serializedObject.FindProperty("_food");
//		_foodManager  = this.serializedObject.FindProperty("_foodManager");
//		_scout = this.serializedObject.FindProperty("_scout");
//		_scoutManager  = this.serializedObject.FindProperty("_scoutManager");
//		_solution  = this.serializedObject.FindProperty("_solution");
			HierarchyError = this.serializedObject.FindProperty ("HierarchyError");
//		_point  = this.serializedObject.FindProperty("_point");

	
		}

		public override void OnInspectorGUI ()
		{
		
		
			if (EditorApplication.isPlaying) {

				EditorGUILayout.Separator ();
				EditorGUILayout.HelpBox ("Cannot edit in playmode", MessageType.Info);
				EditorGUILayout.Separator ();
				//GUI.enabled = false;


			} else {
				GUI.enabled = true;
			
		
				serializedObject.Update ();

				GUIStyle style = new GUIStyle ();
				style.alignment = TextAnchor.MiddleCenter;
				style.fontStyle = FontStyle.Bold;

				style.fontSize = 18;
				EditorGUILayout.Separator ();
				EditorGUILayout.LabelField (_type.enumNames [_type.intValue].ToString () + " Node", style);
				style.fontSize = 12;
				EditorGUILayout.LabelField ("__________________________", style);
				EditorGUILayout.Separator ();			
				EditorGUILayout.PropertyField (ID);
				EditorGUILayout.PropertyField (_type);
				EditorGUILayout.PropertyField (showGizmos);
				if (showGizmos.boolValue)
					EditorGUILayout.PropertyField (_radius);
				style.fontSize = 12;
				EditorGUILayout.LabelField ("__________________________", style);
				EditorGUILayout.Separator ();	

				switch (_type.intValue) {
				case (int)NodeSetup.Tipo.Area:			
//				EditorGUILayout.PropertyField (_area, true);
					EditorGUILayout.Separator ();

					if (GUILayout.Button ("New Food Area", EditorStyles.toolbarButton)) {
						ns.CreateNode (NodeSetup.Tipo.Food);
					}

					if (GUILayout.Button ("New Scout Manager", EditorStyles.toolbarButton)) {
						ns.CreateNode (NodeSetup.Tipo.ScoutManager);
					}

					if (GUILayout.Button ("New Challenge", EditorStyles.toolbarButton)) {
						//	ns.CreateNode (NodeSetup.Tipo.FoodManager);
					}
					EditorGUILayout.Separator ();

					break;	

				case (int)NodeSetup.Tipo.Food:			
//				EditorGUILayout.PropertyField (_food, true);
					EditorGUILayout.Separator ();
					//if (HierarchyError.boolValue)
					//	EditorGUILayout.HelpBox ("Hierarchy is not valid.\nFoodManager not found on Parent", MessageType.Info);
					//if (!HierarchyError.boolValue)
					if (GUILayout.Button ("New Point", EditorStyles.toolbarButton)) {
						ns.CreateNode (NodeSetup.Tipo.Point);
					}
					EditorGUILayout.Separator ();
					break;	


				case (int)NodeSetup.Tipo.FoodManager:
//					EditorGUILayout.PropertyField (_foodManager, true);
					EditorGUILayout.Separator ();
			
					if (GUILayout.Button ("New Food Area", EditorStyles.toolbarButton)) {
						ns.CreateNode (NodeSetup.Tipo.Food);
					}
					EditorGUILayout.Separator ();
	
					break;	

				case (int)NodeSetup.Tipo.Scout:			
//				EditorGUILayout.PropertyField (_scout, true);
					EditorGUILayout.Separator ();
					if (HierarchyError.boolValue)
						EditorGUILayout.HelpBox ("Hierarchy is not valid.\nScoutManager not found on Parent", MessageType.Info);
					break;	

				case (int)NodeSetup.Tipo.ScoutManager:	
//				EditorGUILayout.PropertyField (_scoutManager, true);
					EditorGUILayout.Separator ();

					if (GUILayout.Button ("New Scout Point", EditorStyles.toolbarButton)) {
						ns.CreateNode (NodeSetup.Tipo.Scout);
					}
					EditorGUILayout.Separator ();
	
					break;	
				
				case (int)NodeSetup.Tipo.Solution: 		
//				EditorGUILayout.PropertyField (_solution, true);
					break;	

				case (int)NodeSetup.Tipo.Point: 	
//				EditorGUILayout.PropertyField (_point, true);
					break;	

		

				default:
					break;
				}


			}



			serializedObject.ApplyModifiedProperties ();
			//DrawDefaultInspector ();

		}




	}
}