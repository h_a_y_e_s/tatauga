﻿// Version Incrementor Script for Unity by Francesco Forno (Fornetto Games)
// Inspired by http://forum.unity3d.com/threads/automatic-version-increment-script.144917/

using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;
using hayes;
using System;

[InitializeOnLoad]
public class VersionIncrementor
{
	[PostProcessBuildAttribute(1)]
	public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
	{
		Debug.Log("Build v" + PlayerSettings.bundleVersion + " (" + PlayerSettings.Android.bundleVersionCode + ")");
		IncreaseBuild();
	}

	static void IncrementVersion(int majorIncr, int minorIncr, int buildIncr)
	{
		string[] lines = PlayerSettings.bundleVersion.Split('.');

		int MajorVersion = int.Parse(lines[0]) + majorIncr;
		int MinorVersion = int.Parse(lines[1]) + minorIncr;
		int Build = int.Parse(lines[2]) + buildIncr;

		PlayerSettings.bundleVersion = MajorVersion.ToString("0") + "." +
			MinorVersion.ToString("0") + "." +
			Build.ToString("0");
		PlayerSettings.Android.bundleVersionCode = MajorVersion * 10000 + MinorVersion * 1000 + Build;
		BuildInfo bf = new BuildInfo (PlayerSettings.companyName +"."+
			PlayerSettings.productName +"."+   MajorVersion.ToString() + "." +
			MinorVersion.ToString() + "." +	Build.ToString(), System.DateTime.Now.ToString("MMMM dd yyyy  HH:mm"));
		PersistentData.Save (bf,"version");
//
//		var go = GameObject.FindWithTag ("Version");
//
//		go.GetComponent<DisplayBuild>().name = PlayerSettings.companyName +"."+
//			PlayerSettings.productName +"."+   MajorVersion.ToString() + "." +
//			MinorVersion.ToString() + "." +	Build.ToString();
	}

	[MenuItem("Build/Version/Increase Minor Version")]
	private static void IncreaseMinor()
	{
		IncrementVersion(0, 1, 0);
	}

	[MenuItem("Build/Version/Increase Major Version")]
	private static void IncreaseMajor()
	{
		IncrementVersion(1, 0, 0);
	}

	private static void IncreaseBuild()
	{
		IncrementVersion(0, 0, 1);
	
	}
	[MenuItem("Build/Version/Reset/Reset")]
	private static void Reset()
	{		
		ResetVersion(0, 0, 0);
	}

	private static void ResetVersion(int majorIncr, int minorIncr, int buildIncr)
	{
		string[] lines = "0.0.0".Split('.');

		int MajorVersion = int.Parse(lines[0]) + majorIncr;
		int MinorVersion = int.Parse(lines[1]) + minorIncr;
		int Build = int.Parse(lines[2]) + buildIncr;

		PlayerSettings.bundleVersion = MajorVersion.ToString("0") + "." +
			MinorVersion.ToString("0") + "." +
			Build.ToString("0");
		PlayerSettings.Android.bundleVersionCode = MajorVersion * 10000 + MinorVersion * 1000 + Build;
		BuildInfo bf = new BuildInfo (PlayerSettings.companyName +"."+
			PlayerSettings.productName +"."+   MajorVersion.ToString() + "." +
			MinorVersion.ToString() + "." +	Build.ToString(), System.DateTime.Now.ToString("MMMM dd yyyy  HH:mm"));
		PersistentData.Save (bf,"version");

	}

	[MenuItem("Build/Android/Build and Run")]
	private static void BuildAndRun()
	{
		BuildPipeline.BuildPlayer (EditorBuildSettings.scenes, "C://UnityProjects//Tatauga//Builds//Alpha_" + System.DateTime.Now.ToString ("MMMMdd_yyyy")+".apk", BuildTarget.Android, BuildOptions.AutoRunPlayer);
	}


	[MenuItem("Build/PC/Build and Run")]
	private static void BuildAndRunPC()
	{
		BuildPipeline.BuildPlayer (EditorBuildSettings.scenes, "C://UnityProjects//Tatauga//Builds//Alpha_" + System.DateTime.Now.ToString ("MMMMdd_yyyy")+"", BuildTarget.StandaloneWindows64, BuildOptions.AutoRunPlayer);
	}




}


public class BuildHistory: EditorWindow
{

	[Multiline]
	string myString = "";
	private Vector2 scrollPosition;
	public string textToDisplay;
	GUIStyle style;


	[MenuItem ("Build/Version/Add Note")]
	static void Init () 
	{
		BuildHistory window = (BuildHistory)EditorWindow.GetWindow (typeof (BuildHistory));
		window.Show();
		window.position = new Rect(0,0,500, 250);

	}

	void OnGUI()
	{		
		
		//GUILayout.Label ("Last changes:", EditorStyles.boldLabel);
		//myString = EditorGUI.TextArea (new Rect (10, 10, position.width -10, 30), "Last change: ");
		GUI.skin.textField.wordWrap = true;
		scrollPosition = GUILayout.BeginScrollView(scrollPosition, GUILayout.Width(position.width-10), GUILayout.Height(100));
		myString = EditorGUI.TextField (new Rect (10, 10, position.width -10, position.height), myString);
		GUILayout.EndScrollView();

	

		if (GUI.Button (new Rect (10, 70, 50, 30), "Ok")) 
		{
			
		}
			
	}
	
}