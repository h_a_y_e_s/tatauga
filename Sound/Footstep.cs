﻿namespace hayes
{
using UnityEngine;
using System.Collections;

public class Footstep : MonoBehaviour
{	


		public GroundType currentGround;
		public GameObject FootR;
		public GameObject FootL;
		public InAudioEvent[] GroundTypeAudioEvents;

		public void OnFootStepR()
		{
			int x = (int) currentGround;
			if(GroundTypeAudioEvents[x] !=null) InAudio.PostEvent ( FootR, GroundTypeAudioEvents[x]);
		}

		public void OnFootStepL()
		{
			int x = (int) currentGround;
			if(GroundTypeAudioEvents[x] !=null) InAudio.PostEvent ( FootL, GroundTypeAudioEvents[x]);
		}

		void OnTriggerEnter(Collider c)
		{

			if (c.gameObject.layer.Equals (LayerMask.NameToLayer ("Ground"))) 
			{
				switch (c.gameObject.tag)
				{
				case "GroudGrass":
					currentGround = GroundType.Grass;
					break;
				case "GroundWood":
					currentGround = GroundType.Wood;
					break;
				case "GroundMetal":
					currentGround = GroundType.Metal;
					break;
				case "GroundRocks":
					currentGround = GroundType.Grass;
					break;
				default:currentGround = GroundType.Grass;
					break;
				}
			}
		}

}

	public enum GroundType
	{
		Grass = 0,
		Wood = 1,
		Metal = 2
	}
}
