﻿namespace hayes
{
	using UnityEngine;
	using UnityEditor;
	using System.Collections;

	[CustomEditor (typeof(FoodPrefabMaker))]
	public class FoodPrefabEditor : Editor
	{

		public override void OnInspectorGUI ()
		{
			var ns = (FoodPrefabMaker) target;

			if (GUILayout.Button ("Save to file")) 
			{
				ns.SaveToJson ();
			}
			DrawDefaultInspector ();
		}

	}
}